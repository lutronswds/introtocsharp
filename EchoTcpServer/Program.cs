﻿using System;
using System.Configuration;
using System.Globalization;
using System.Net;
using System.Net.Sockets;

namespace Lutron.Swds.IntroToCSharp.EchoTcpServer
{

    class Program
    {

        #region Constants

        /// <summary>
        ///  Maximum number of bytes to receive at once
        /// </summary>
        const int bufferSize = 1024;

        #endregion

        /// <summary>
        /// Main entry point for the application
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters", MessageId = "System.Console.WriteLine(System.String)")]
        static void Main()
        {
            // Create a TCP listener to listen for connections. It will listen for incoming connections
            // at the port read from the configuration file on every NIC
            TcpListener tcpListener = new TcpListener(IPAddress.Any,
                                                      int.Parse(ConfigurationManager.AppSettings["ServerPort"],
                                                                CultureInfo.InvariantCulture));
            
            // Actually start the listener
            tcpListener.Start();

            // Run the Async function that will accept the incoming connections
            ListenForConnectionsAsync(tcpListener);

            Console.WriteLine("Press the <Enter> key to end.");

            Console.ReadLine();
        }

        /// <summary>
        /// Async function that listens for incoming connections and starts the echo routine
        /// on the new connection
        /// </summary>
        /// <param name="tcpListener">Already started TCP Listener</param>
        static async void ListenForConnectionsAsync(TcpListener tcpListener)
        {
            while (true)
            {
                // This line will block until there is an incoming connection
                TcpClient tcpClient = await tcpListener.AcceptTcpClientAsync();
                Console.WriteLine("Connection from " + tcpClient.Client.RemoteEndPoint);

                // Start up the echo routine on the new connection
                EchoDataAsync(tcpClient);
            }
        }

        /// <summary>
        /// Async function that simply receives data from the connected client and
        /// writes it back
        /// </summary>
        /// <param name="tcpClient">Connected TCP Client</param>
        static async void EchoDataAsync(TcpClient tcpClient)
        {
            try
            {
                NetworkStream networkStream = tcpClient.GetStream();
                byte[] buffer = new byte[bufferSize];

                while (true)
                {
                    int numberOfBytesReceived = await networkStream.ReadAsync(buffer, 0, bufferSize);
                    //TODO: Right now the server simply echos back whatever it receives from the client.
                    //      You can make it do something more interesting by processing what the bytes
                    //      that have been received, adding some logic, and then changing what bytes
                    //      are written to the stream.
                    await networkStream.WriteAsync(buffer, 0, numberOfBytesReceived);
                }
            }
            catch (Exception)
            {
                // Any exceptions that get thrown means that the stream (and the underlying socket)
                // has been closed
                Console.WriteLine("Disconnect");
            }
        }

    }

}
