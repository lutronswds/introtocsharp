﻿using System;
using System.Configuration;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace Lutron.Swds.IntroToCSharp.TcpClient
{

    public partial class TcpClientForm : Form
    {

        #region Constants

        /// <summary>
        ///  Maximum number of bytes to receive at once
        /// </summary>
        const int bufferSize = 1024;

        #endregion

        #region Fields

        /// <summary>
        /// Network stream to read and write to
        /// </summary>
        NetworkStream networkStream;

        /// <summary>
        /// Buffer to receive bytes into
        /// </summary>
        byte[] receiveBuffer;

        /// <summary>
        /// Holds the received text for display
        /// </summary>
        StringBuilder stringBuilder;

        /// <summary>
        /// The TCP Client that will be used for communications
        /// </summary>
        System.Net.Sockets.TcpClient tcpClient;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor for the form, just allocates
        /// some of the field variables
        /// </summary>
        public TcpClientForm()
        {
            InitializeComponent();
            receiveBuffer = new byte[bufferSize];
            stringBuilder = new StringBuilder();
        }

        #endregion

        #region Form Events

        /// <summary>
        /// Event handler for the Form_Load event
        /// </summary>
        /// <param name="sender">Object that fired the event</param>
        /// <param name="e">Generic event args</param>
        private void TcpClientForm_Load(object sender, EventArgs e)
        {
            // Read the default values from the configuration file
            serverAddressTextBox.Text = ConfigurationManager.AppSettings["ServerAddress"];
            serverPortTextBox.Text = ConfigurationManager.AppSettings["ServerPort"];

            EnableControls();
        }

        #endregion

        #region Control Events

        /// <summary>
        /// Event handler for serverAddressTextBox TextChanged event
        /// Just enables/disables other controls
        /// </summary>
        /// <param name="sender">Object that fired the event</param>
        /// <param name="e">Generic event args</param>
        private void serverAddressTextBox_TextChanged(object sender, EventArgs e)
        {
            EnableControls();
        }

        /// <summary>
        /// Event handler for serverPortTextBox TextChanged event
        /// Just enables/disables other controls
        /// </summary>
        /// <param name="sender">Object that fired the event</param>
        /// <param name="e">Generic event args</param>
        private void serverPortTextBox_TextChanged(object sender, EventArgs e)
        {
            EnableControls();
        }

        /// <summary>
        /// Event handler for the connectionConnectDisconnectButton Click event
        /// Takes care of acutally connecting or disconnecting to the server
        /// </summary>
        /// <param name="sender">Object that fired the event</param>
        /// <param name="e">Generic event args</param>
        private void connectionConnectDisconnectButton_Click(object sender, EventArgs e)
        {
            if (tcpClient != null && tcpClient.Connected)
            {
                // We are currently connected, disconnect
                tcpClient.Close();
                EnableControls();
            }
            else
            {
                // We are not connected try to connect asynchronously
                connectionConnectDisconnectButton.Enabled = false;
                tcpClient = new System.Net.Sockets.TcpClient();
                tcpClient.BeginConnect(serverAddressTextBox.Text,
                                       int.Parse(serverPortTextBox.Text,
                                                 CultureInfo.CurrentCulture),
                                       asyncResult =>
                                       {
                                           tcpClient.EndConnect(asyncResult);

                                           networkStream = tcpClient.GetStream();
                                           StartReceiving();
                                           EnableControls();
                                       },
                                       null);
            }
        }

        /// <summary>
        /// Event handler for the textToSendTextBox TextChanged event
        /// Just enables/disables other controls
        /// </summary>
        /// <param name="sender">Object that fired the event</param>
        /// <param name="e">Generic event args</param>
        private void textToSendTextBox_TextChanged(object sender, EventArgs e)
        {
            EnableControls();
        }

        /// <summary>
        /// Event handler for the sendTextButton Click event
        /// Actually writes the entered text to the network stream
        /// </summary>
        /// <param name="sender">Object that fired the event</param>
        /// <param name="e">Generic event args</param>
        private void sendTextButton_Click(object sender, EventArgs e)
        {
            byte[] buffer = Encoding.UTF8.GetBytes(textToSendTextBox.Text);
            networkStream.Write(buffer, 0, buffer.Length);
        }

        #endregion

        #region Override Methods

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            if (disposing && (tcpClient != null))
            {
                tcpClient.Close();
                tcpClient = null;
            }
            base.Dispose(disposing);
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Appends the passed in data string to the receive text label
        /// </summary>
        /// <param name="data">String to append</param>
        void DisplayReceivedData(string data)
        {
            if (InvokeRequired)
            {
                // When called from a non-GUI thread Invoke must be called
                // on this function so it runs in a GUI thread
                Invoke(new Action<string>(DisplayReceivedData), data);
            }
            else
            {
                stringBuilder.Append(data);
                receiveTextLabel.Text = stringBuilder.ToString();
            }
        }

        /// <summary>
        /// All of the logic for enable and disabling the controls on the form, also takes
        /// care of the text on the connect/disconnect button
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters", MessageId = "System.Windows.Forms.Control.set_Text(System.String)")]
        void EnableControls()
        {
            if (InvokeRequired)
            {
                // When called from a non-GUI thread Invoke must be called
                // on this function so it runs in a GUI thread
                Invoke(new Action(EnableControls));
            }
            else
            {
                bool connectionAllowed = !string.IsNullOrWhiteSpace(serverAddressTextBox.Text) &&
                                         !string.IsNullOrWhiteSpace(serverPortTextBox.Text);
                bool connected = connectionAllowed && tcpClient != null && tcpClient.Connected;
                bool textReadyToSend = connected && !string.IsNullOrWhiteSpace(textToSendTextBox.Text);

                serverAddressTextBox.Enabled = connected;
                serverPortTextBox.Enabled = connected;
                connectionConnectDisconnectButton.Enabled = connectionAllowed;
                if (connected)
                {
                    connectionConnectDisconnectButton.Text = "Disconnect";
                }
                else
                {
                    connectionConnectDisconnectButton.Text = "Connect";
                }
                textToSendTextBox.Enabled = connected;
                sendTextButton.Enabled = textReadyToSend;
            }
        }

        /// <summary>
        /// Receives the data from the server asynchronously
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")]
        void StartReceiving()
        {
            networkStream.BeginRead(receiveBuffer,
                                    0,
                                    bufferSize,
                                    asyncResult =>
                                    {
                                        try
                                        {
                                            //TODO: Make this do something so that received data can be displayed
                                            //      Hint: EndRead returns an integer with the number of bytes read
                                            networkStream.EndRead(asyncResult);
                                            StartReceiving();
                                        }
                                        catch (Exception)
                                        {
                                            // Intentionally empty
                                        }
                                    },
                                    null);
        }

        #endregion

    }

}
