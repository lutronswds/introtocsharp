﻿namespace Lutron.Swds.IntroToCSharp.TcpClient
{
    partial class TcpClientForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters", MessageId = "System.Windows.Forms.Control.set_Text(System.String)")]
        private void InitializeComponent()
        {
            this.tcpConnectionGroupBox = new System.Windows.Forms.GroupBox();
            this.connectionConnectDisconnectButton = new System.Windows.Forms.Button();
            this.serverPortTextBox = new System.Windows.Forms.TextBox();
            this.serverPortLabel = new System.Windows.Forms.Label();
            this.serverAddressTextBox = new System.Windows.Forms.TextBox();
            this.serverAddressLabel = new System.Windows.Forms.Label();
            this.transmitGroupBox = new System.Windows.Forms.GroupBox();
            this.sendTextButton = new System.Windows.Forms.Button();
            this.textToSendTextBox = new System.Windows.Forms.TextBox();
            this.typeTextToSendLabel = new System.Windows.Forms.Label();
            this.receiveGroupBox = new System.Windows.Forms.GroupBox();
            this.receiveTextLabel = new System.Windows.Forms.Label();
            this.tcpConnectionGroupBox.SuspendLayout();
            this.transmitGroupBox.SuspendLayout();
            this.receiveGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // tcpConnectionGroupBox
            // 
            this.tcpConnectionGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcpConnectionGroupBox.Controls.Add(this.connectionConnectDisconnectButton);
            this.tcpConnectionGroupBox.Controls.Add(this.serverPortTextBox);
            this.tcpConnectionGroupBox.Controls.Add(this.serverPortLabel);
            this.tcpConnectionGroupBox.Controls.Add(this.serverAddressTextBox);
            this.tcpConnectionGroupBox.Controls.Add(this.serverAddressLabel);
            this.tcpConnectionGroupBox.Location = new System.Drawing.Point(12, 12);
            this.tcpConnectionGroupBox.Name = "tcpConnectionGroupBox";
            this.tcpConnectionGroupBox.Size = new System.Drawing.Size(727, 106);
            this.tcpConnectionGroupBox.TabIndex = 0;
            this.tcpConnectionGroupBox.TabStop = false;
            this.tcpConnectionGroupBox.Text = "TCP Connection";
            // 
            // connectionConnectDisconnectButton
            // 
            this.connectionConnectDisconnectButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.connectionConnectDisconnectButton.Location = new System.Drawing.Point(6, 72);
            this.connectionConnectDisconnectButton.Name = "connectionConnectDisconnectButton";
            this.connectionConnectDisconnectButton.Size = new System.Drawing.Size(715, 23);
            this.connectionConnectDisconnectButton.TabIndex = 4;
            this.connectionConnectDisconnectButton.Text = "Connect";
            this.connectionConnectDisconnectButton.UseVisualStyleBackColor = true;
            this.connectionConnectDisconnectButton.Click += new System.EventHandler(this.connectionConnectDisconnectButton_Click);
            // 
            // serverPortTextBox
            // 
            this.serverPortTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.serverPortTextBox.Location = new System.Drawing.Point(94, 46);
            this.serverPortTextBox.Name = "serverPortTextBox";
            this.serverPortTextBox.Size = new System.Drawing.Size(627, 20);
            this.serverPortTextBox.TabIndex = 3;
            this.serverPortTextBox.TextChanged += new System.EventHandler(this.serverPortTextBox_TextChanged);
            // 
            // serverPortLabel
            // 
            this.serverPortLabel.AutoSize = true;
            this.serverPortLabel.Location = new System.Drawing.Point(6, 49);
            this.serverPortLabel.Name = "serverPortLabel";
            this.serverPortLabel.Size = new System.Drawing.Size(63, 13);
            this.serverPortLabel.TabIndex = 2;
            this.serverPortLabel.Text = "Server Port:";
            // 
            // serverAddressTextBox
            // 
            this.serverAddressTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.serverAddressTextBox.Location = new System.Drawing.Point(94, 20);
            this.serverAddressTextBox.Name = "serverAddressTextBox";
            this.serverAddressTextBox.Size = new System.Drawing.Size(627, 20);
            this.serverAddressTextBox.TabIndex = 1;
            this.serverAddressTextBox.TextChanged += new System.EventHandler(this.serverAddressTextBox_TextChanged);
            // 
            // serverAddressLabel
            // 
            this.serverAddressLabel.AutoSize = true;
            this.serverAddressLabel.Location = new System.Drawing.Point(6, 23);
            this.serverAddressLabel.Name = "serverAddressLabel";
            this.serverAddressLabel.Size = new System.Drawing.Size(82, 13);
            this.serverAddressLabel.TabIndex = 0;
            this.serverAddressLabel.Text = "Server Address:";
            // 
            // transmitGroupBox
            // 
            this.transmitGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.transmitGroupBox.Controls.Add(this.sendTextButton);
            this.transmitGroupBox.Controls.Add(this.textToSendTextBox);
            this.transmitGroupBox.Controls.Add(this.typeTextToSendLabel);
            this.transmitGroupBox.Location = new System.Drawing.Point(12, 124);
            this.transmitGroupBox.Name = "transmitGroupBox";
            this.transmitGroupBox.Size = new System.Drawing.Size(727, 126);
            this.transmitGroupBox.TabIndex = 1;
            this.transmitGroupBox.TabStop = false;
            this.transmitGroupBox.Text = "Transmit";
            // 
            // sendTextButton
            // 
            this.sendTextButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sendTextButton.Location = new System.Drawing.Point(6, 97);
            this.sendTextButton.Name = "sendTextButton";
            this.sendTextButton.Size = new System.Drawing.Size(715, 23);
            this.sendTextButton.TabIndex = 2;
            this.sendTextButton.Text = "Send Text";
            this.sendTextButton.UseVisualStyleBackColor = true;
            this.sendTextButton.Click += new System.EventHandler(this.sendTextButton_Click);
            // 
            // textToSendTextBox
            // 
            this.textToSendTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textToSendTextBox.Location = new System.Drawing.Point(6, 32);
            this.textToSendTextBox.Multiline = true;
            this.textToSendTextBox.Name = "textToSendTextBox";
            this.textToSendTextBox.Size = new System.Drawing.Size(715, 59);
            this.textToSendTextBox.TabIndex = 1;
            this.textToSendTextBox.TextChanged += new System.EventHandler(this.textToSendTextBox_TextChanged);
            // 
            // typeTextToSendLabel
            // 
            this.typeTextToSendLabel.AutoSize = true;
            this.typeTextToSendLabel.Location = new System.Drawing.Point(6, 16);
            this.typeTextToSendLabel.Name = "typeTextToSendLabel";
            this.typeTextToSendLabel.Size = new System.Drawing.Size(92, 13);
            this.typeTextToSendLabel.TabIndex = 0;
            this.typeTextToSendLabel.Text = "Type text to send:";
            // 
            // receiveGroupBox
            // 
            this.receiveGroupBox.Controls.Add(this.receiveTextLabel);
            this.receiveGroupBox.Location = new System.Drawing.Point(12, 256);
            this.receiveGroupBox.Name = "receiveGroupBox";
            this.receiveGroupBox.Size = new System.Drawing.Size(727, 230);
            this.receiveGroupBox.TabIndex = 2;
            this.receiveGroupBox.TabStop = false;
            this.receiveGroupBox.Text = "Receive";
            // 
            // receiveTextLabel
            // 
            this.receiveTextLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.receiveTextLabel.Location = new System.Drawing.Point(6, 16);
            this.receiveTextLabel.Name = "receiveTextLabel";
            this.receiveTextLabel.Size = new System.Drawing.Size(715, 211);
            this.receiveTextLabel.TabIndex = 0;
            // 
            // TcpClientForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(751, 498);
            this.Controls.Add(this.receiveGroupBox);
            this.Controls.Add(this.transmitGroupBox);
            this.Controls.Add(this.tcpConnectionGroupBox);
            this.Name = "TcpClientForm";
            this.Text = "TCP Client";
            this.Load += new System.EventHandler(this.TcpClientForm_Load);
            this.tcpConnectionGroupBox.ResumeLayout(false);
            this.tcpConnectionGroupBox.PerformLayout();
            this.transmitGroupBox.ResumeLayout(false);
            this.transmitGroupBox.PerformLayout();
            this.receiveGroupBox.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox tcpConnectionGroupBox;
        private System.Windows.Forms.Button connectionConnectDisconnectButton;
        private System.Windows.Forms.TextBox serverPortTextBox;
        private System.Windows.Forms.Label serverPortLabel;
        private System.Windows.Forms.TextBox serverAddressTextBox;
        private System.Windows.Forms.Label serverAddressLabel;
        private System.Windows.Forms.GroupBox transmitGroupBox;
        private System.Windows.Forms.Button sendTextButton;
        private System.Windows.Forms.TextBox textToSendTextBox;
        private System.Windows.Forms.Label typeTextToSendLabel;
        private System.Windows.Forms.GroupBox receiveGroupBox;
        private System.Windows.Forms.Label receiveTextLabel;
    }
}

