ReadMe.txt

Simple Exercise: Calculator
---------------------------
1) Given is a simple calculator which can only handle 2 operands at a time. 
2) Also, there is no error checking in place. And after a correct two operand expression calculated, 
   the UI will force you to clear current result. If you hate this, change it. :)

Exercise Items to complete:

1) Add buttons for numbers 7, 8, 9.
2) Add division operation.
3) Make all fonts on the buttons bold.
4) Make the text box read only.
5) Change fonts to Courier New 12 pt for numbers on buttons.
6) Change the color of "C" on the clear button to white. Make the clear button's color Red.
7) Handle division by zero error using the custom exception in the project.
8) Homework task: 
                Handle sophisticated expressions like 1+2+3-5*2+1/4+1 = -2.75 by rewriting Evaluate() in Calculator.cs                   
                This is pretty much open-ended. So, you can go as sophisticated as you like.
                                
                We will publish our version of solutions for your reference at next SWDS.
                This is to give you an opportunity to try it yourself first.

Advanced Exercise: Client-Server Communications
-----------------------------------------------
1) In the TcpClient project, in TcpClientForm::StartReceiving the
   EndRead function is called and then the StartReceiving function
   is immediately called. There is no code to process or display
   any of the received data. Fix this.
2) In the EchoTcpServer project, in Program::EchoDataAsync the
   data received is just written back to the network stream.
   Add some logic to actually create a protocol (i.e. look for
   a message terminator, parse the received message, send the
   appropriate response, etc.)
