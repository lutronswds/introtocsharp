﻿using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lutron.Swds.IntroToCSharp.PresentationCode.UnitTests
{

    [TestClass]
    public class ExtensionMethodDemoTest
    {

        [TestMethod]
        public void ToBinaryString_Int_Zero_Test()
        {
            string expectedValue = new string('0', 31);

            Assert.AreEqual<string>(expectedValue,
                                    0.ToBinaryString());
        }

        [TestMethod]
        public void ToBinaryString_Int_PositiveIntegers_Test()
        {
            Assert.AreEqual<string>("0000000000000000000000000000001",
                                    1.ToBinaryString());

            Assert.AreEqual<string>("0000000000000000000000000000010",
                                    2.ToBinaryString());

            Assert.AreEqual<string>("0000000000000000000000000000011",
                                    3.ToBinaryString());

            Assert.AreEqual<string>("0000000000000000000000001100100",
                                    100.ToBinaryString());
        }

        [TestMethod]
        public void ToBinaryString_Int_NegativeIntegers_Test()
        {
            Assert.AreEqual<string>("-0000000000000000000000000000001",
                                    (-1).ToBinaryString());

            Assert.AreEqual<string>("-0000000000000000000000000000010",
                                    (-2).ToBinaryString());

            Assert.AreEqual<string>("-0000000000000000000000000000011",
                                    (-3).ToBinaryString());

            Assert.AreEqual<string>("-0000000000000000000000001100100",
                                    (-100).ToBinaryString());
        }

    }

}
