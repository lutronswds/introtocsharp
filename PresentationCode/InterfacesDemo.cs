﻿using System;

namespace Lutron.Swds.IntroToCSharp.PresentationCode
{

    public interface IDemo
    {

        int Value { get; set; }

    }

    public class InterfacesDemo : IDemo, IComparable<int>
    {

        public InterfacesDemo(int value)
        {
            Value = value;
        }

        public int Value { get; set; }

        public int CompareTo(int other)
        {
            return Value.CompareTo(other);
        }

    }

}
