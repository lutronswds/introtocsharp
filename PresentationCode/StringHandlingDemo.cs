﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Lutron.Swds.IntroToCSharp.PresentationCode
{

    public class StringHandlingDemo
    {

        #region Build String

        public string BuildStringBad(IEnumerable<DemoValue> demoValues)
        {
            string returnValue = String.Empty;

            foreach (DemoValue demoValue in demoValues)
            {
                returnValue += demoValue.StringValue;
            }

            return returnValue;
        }

        public string BuildStringGood(IEnumerable<DemoValue> demoValues)
        {
            StringBuilder stringBuilder = new StringBuilder();

            foreach (DemoValue demoValue in demoValues)
            {
                stringBuilder.Append(demoValue.StringValue);
            }

            return stringBuilder.ToString();
        }

        #endregion

        #region File IO

        public void WriteToFile(string filePath, DemoValue demoValue)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(demoValue.StringValue);
            File.WriteAllBytes(filePath, bytes);
        }

        public string ReadFromFile(string filePath)
        {
            byte[] bytes = File.ReadAllBytes(filePath);
            return Encoding.UTF8.GetString(bytes);
        }

        #endregion

    }

}
