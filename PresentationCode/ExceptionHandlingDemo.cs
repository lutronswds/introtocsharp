﻿using System;

namespace Lutron.Swds.IntroToCSharp.PresentationCode
{

    public class ExceptionHandlingDemo
    {
        const int maxAllowableSum = 100;

        public void MethodThatCatchesExceptions(DemoValue demoValue)
        {
            try
            {
                long sumValue = MethodThatCanThrowExceptions(demoValue);
                System.Diagnostics.Debug.WriteLine("Sum = {0}", sumValue);
            }
            catch (ArgumentNullException argumentNullException)
            {
                System.Diagnostics.Debug.WriteLine("ArgumentNullException thrown: {0}. Parameter {1} was null",
                                                   argumentNullException.Message,
                                                   argumentNullException.ParamName);
            }
            catch (SumTooLargeException sumTooLargeException)
            {
                System.Diagnostics.Debug.WriteLine("SumTooLargeException thrown: {0}. Sum Value = {1}",
                                                   sumTooLargeException.Message,
                                                   sumTooLargeException.SumValue);
                throw;
            }
            finally
            {
                demoValue.Value1 = 0;
                demoValue.Value2 = 0;
            }
        }

        long MethodThatCanThrowExceptions(DemoValue demoValue)
        {
            if (demoValue == null)
            {
                throw new ArgumentNullException("demoValue");
            }

            long sum = demoValue.Value1 + demoValue.Value2;
            if (sum > maxAllowableSum)
            {
                throw new SumTooLargeException(string.Format("Sum is greater than Max Allowable Sum({0})", 
                                                             maxAllowableSum), 
                                               sum);
            }

            return sum;
        }

    }

    public class SumTooLargeException : Exception
    {

        public SumTooLargeException(string message, long sumValue)
            : base(message)
        {
            SumValue = sumValue;
        }

        public long SumValue { get; set; }

    }

}
