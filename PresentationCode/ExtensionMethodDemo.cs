﻿using System;
using System.Text;

namespace Lutron.Swds.IntroToCSharp.PresentationCode
{

    public static class ExtensionMethodDefinition
    {

        public static string ToBinaryString(this int intValue)
        {
            StringBuilder stringBuilder = new StringBuilder();

            int valueToUse = intValue;
            if (intValue < 0)
            {
                stringBuilder.Append("-");
                valueToUse = -1 * intValue;
            }
            for (int index = 30; index >= 0; index--)
            {
                stringBuilder.Append((valueToUse & (int)Math.Pow(2, index)) >> index);
            }

            return stringBuilder.ToString();
        }

    }

    public class ExtensionMethodDemo
    {

        public string GetBinaryString(int value)
        {
            return value.ToBinaryString();
        }

        public string GetOneBinaryString()
        {
            return 1.ToBinaryString();
        }

    }

}
