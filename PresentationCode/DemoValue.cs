﻿using System.Globalization;

namespace Lutron.Swds.IntroToCSharp.PresentationCode
{

    public class DemoValue
    {

        #region Constructors

        public DemoValue()
        {
            // Intentionally empty
        }

        public DemoValue(int value1, int value2, string stringValue)
            :this()
        {
            Value1 = value1;
            Value2 = value2;
            StringValue = stringValue;
        }

        #endregion

        #region Properties

        public int Value1 { get; set; }

        public int Value2 { get; set; }

        public string StringValue { get; set; }

        #endregion

        #region Override Methods

        public override string ToString()
        {
            return string.Format(CultureInfo.CurrentCulture,
                                 "Value1: {0}, Value2: {1}, StringValue: {2}", 
                                 Value1, 
                                 Value2,
                                 StringValue);
        }

        #endregion

    }

}
