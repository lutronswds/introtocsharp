﻿using System.Collections.Generic;

namespace Lutron.Swds.IntroToCSharp.PresentationCode
{

    public class IteratorPatternDemo
    {

        public int CreateSum1(List<DemoValue> demoValueList)
        {
            int returnValue = 0;

            foreach (DemoValue demoValue in demoValueList)
            {
                returnValue += demoValue.Value1;
            }

            return returnValue;
        }

        public int CreateSum2(IEnumerable<DemoValue> demoValues)
        {
            int returnValue = 0;

            foreach (DemoValue demoValue in demoValues)
            {
                returnValue += demoValue.Value2;
            }

            return returnValue;
        }

    }

}
