﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Lutron.Swds.IntroToCSharp.PresentationCode
{

    public class LambdaDemo
    {

        public IEnumerable<DemoValue> FilterByMinValue(IEnumerable<DemoValue> demoValues, 
                                                       int minValue)
        {
            return demoValues.Where(dV => (dV.Value1 + dV.Value2) >= minValue);
        }

        public void RunInBackgroundThread(IEnumerable<DemoValue> demoValues)
        {
            Thread backgroundThread = new Thread(() =>
                                                 {
                                                     foreach (DemoValue demoValue in demoValues)
                                                     {
                                                         ExecuteReallyLongOperation(demoValue);
                                                     }
                                                 });
            backgroundThread.IsBackground = true;
            backgroundThread.Start();
        }

        void ExecuteReallyLongOperation(DemoValue demoValue)
        {
            Thread.Sleep(60 * 1000);
            System.Diagnostics.Debug.WriteLine("DemoValue: {0}" + demoValue.ToString());
        }

    }

}
