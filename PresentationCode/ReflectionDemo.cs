﻿using System;
using System.Linq;
using System.Reflection;

namespace Lutron.Swds.IntroToCSharp.PresentationCode
{

    public class ReflectionDemo
    {

        public IDemo CreateInstance(string assemblyFilePath)
        {
            Assembly assembly = Assembly.LoadFrom(assemblyFilePath);

            Type typeToLoad = (from type in assembly.GetTypes()
                               where type.IsClass && type.GetInterfaces().Contains(typeof(IDemo))
                               select type).FirstOrDefault();
            if (typeToLoad == null)
            {
                return null;
            }

            return (IDemo)Activator.CreateInstance(typeToLoad);
        }

    }

}
