﻿using System.Collections.Generic;

namespace Lutron.Swds.IntroToCSharp.PresentationCode
{

    public class GenericsDemo
    {
        GenericClass<DemoValue> values;

        public GenericsDemo()
        {
            values = new GenericClass<DemoValue>();
            values.Add(new DemoValue());
        }

    }

    public class GenericClass<T> : IEnumerable<T>
    {
        List<T> items;

        public GenericClass()
        {
            items = new List<T>();
        }

        public T Add(T newItem)
        {
            items.Add(newItem);
            return newItem;
        }

        public IEnumerable<T> Add(IEnumerable<T> newItems)
        {
            items.AddRange(newItems);
            return items;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return items.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return items.GetEnumerator();
        }
    }

}
