﻿using System;

namespace Lutron.Swds.IntroToCSharp.PresentationCode
{

    public class ObservableClass
    {

        public event EventHandler<ChangedEventArgs> Changed;

        public void OnChanged(int newValue)
        {
            if (Changed != null)
            {
                Changed(this,
                        new ChangedEventArgs
                        { 
                            NewValue = newValue 
                        });
            }
        }

    }

    public class ObserverPatternDemo
    {

        public void SubscribeToEvent(ObservableClass observableClass)
        {
            observableClass.Changed += observableClass_Changed;
        }

        void observableClass_Changed(object sender, ChangedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("NewValue = {0}", e.NewValue);
        }

    }

    public class ChangedEventArgs : EventArgs
    {

        public int NewValue { get; set; }

    }

}
