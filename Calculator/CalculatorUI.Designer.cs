﻿namespace Lutron.Swds.IntroToCSharp.Calculator {
    partial class CalculatorUI {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.display = new System.Windows.Forms.RichTextBox();
            this.key_1 = new System.Windows.Forms.Button();
            this.key_2 = new System.Windows.Forms.Button();
            this.key_3 = new System.Windows.Forms.Button();
            this.key_4 = new System.Windows.Forms.Button();
            this.key_5 = new System.Windows.Forms.Button();
            this.key_6 = new System.Windows.Forms.Button();
            this.key_0 = new System.Windows.Forms.Button();
            this.point_key = new System.Windows.Forms.Button();
            this.plus_key = new System.Windows.Forms.Button();
            this.minus_key = new System.Windows.Forms.Button();
            this.multiply_key = new System.Windows.Forms.Button();
            this.equal_key = new System.Windows.Forms.Button();
            this.backspace_key = new System.Windows.Forms.Button();
            this.clear_key = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // display
            // 
            this.display.Font = new System.Drawing.Font("Courier New", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.display.Location = new System.Drawing.Point(8, 22);
            this.display.Name = "display";
            this.display.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.display.Size = new System.Drawing.Size(246, 39);
            this.display.TabIndex = 0;
            this.display.Text = "";
            // 
            // key_1
            // 
            this.key_1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.key_1.Location = new System.Drawing.Point(17, 175);
            this.key_1.Name = "key_1";
            this.key_1.Size = new System.Drawing.Size(40, 40);
            this.key_1.TabIndex = 1;
            this.key_1.Text = "1";
            this.key_1.UseVisualStyleBackColor = true;
            this.key_1.Click += new System.EventHandler(this.key_1_Click);
            // 
            // key_2
            // 
            this.key_2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.key_2.Location = new System.Drawing.Point(63, 175);
            this.key_2.Name = "key_2";
            this.key_2.Size = new System.Drawing.Size(40, 40);
            this.key_2.TabIndex = 2;
            this.key_2.Text = "2";
            this.key_2.UseVisualStyleBackColor = true;
            this.key_2.Click += new System.EventHandler(this.key_2_Click);
            // 
            // key_3
            // 
            this.key_3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.key_3.Location = new System.Drawing.Point(109, 175);
            this.key_3.Name = "key_3";
            this.key_3.Size = new System.Drawing.Size(40, 40);
            this.key_3.TabIndex = 3;
            this.key_3.Text = "3";
            this.key_3.UseVisualStyleBackColor = true;
            this.key_3.Click += new System.EventHandler(this.key_3_Click);
            // 
            // key_4
            // 
            this.key_4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.key_4.Location = new System.Drawing.Point(17, 129);
            this.key_4.Name = "key_4";
            this.key_4.Size = new System.Drawing.Size(40, 40);
            this.key_4.TabIndex = 4;
            this.key_4.Text = "4";
            this.key_4.UseVisualStyleBackColor = true;
            this.key_4.Click += new System.EventHandler(this.key_4_Click);
            // 
            // key_5
            // 
            this.key_5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.key_5.Location = new System.Drawing.Point(63, 129);
            this.key_5.Name = "key_5";
            this.key_5.Size = new System.Drawing.Size(40, 40);
            this.key_5.TabIndex = 5;
            this.key_5.Text = "5";
            this.key_5.UseVisualStyleBackColor = true;
            this.key_5.Click += new System.EventHandler(this.key_5_Click);
            // 
            // key_6
            // 
            this.key_6.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.key_6.Location = new System.Drawing.Point(109, 129);
            this.key_6.Name = "key_6";
            this.key_6.Size = new System.Drawing.Size(40, 40);
            this.key_6.TabIndex = 6;
            this.key_6.Text = "6";
            this.key_6.UseVisualStyleBackColor = true;
            this.key_6.Click += new System.EventHandler(this.key_6_Click);
            // 
            // key_0
            // 
            this.key_0.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.key_0.Location = new System.Drawing.Point(17, 221);
            this.key_0.Name = "key_0";
            this.key_0.Size = new System.Drawing.Size(86, 40);
            this.key_0.TabIndex = 10;
            this.key_0.Text = "0";
            this.key_0.UseVisualStyleBackColor = true;
            this.key_0.Click += new System.EventHandler(this.key_0_Click);
            // 
            // point_key
            // 
            this.point_key.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.point_key.Location = new System.Drawing.Point(109, 221);
            this.point_key.Name = "point_key";
            this.point_key.Size = new System.Drawing.Size(40, 40);
            this.point_key.TabIndex = 11;
            this.point_key.Text = ".";
            this.point_key.UseVisualStyleBackColor = true;
            this.point_key.Click += new System.EventHandler(this.point_key_Click);
            // 
            // plus_key
            // 
            this.plus_key.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.plus_key.Location = new System.Drawing.Point(168, 221);
            this.plus_key.Name = "plus_key";
            this.plus_key.Size = new System.Drawing.Size(40, 40);
            this.plus_key.TabIndex = 12;
            this.plus_key.Text = "+";
            this.plus_key.UseVisualStyleBackColor = true;
            this.plus_key.Click += new System.EventHandler(this.plus_key_Click);
            // 
            // minus_key
            // 
            this.minus_key.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.minus_key.Location = new System.Drawing.Point(168, 175);
            this.minus_key.Name = "minus_key";
            this.minus_key.Size = new System.Drawing.Size(40, 40);
            this.minus_key.TabIndex = 13;
            this.minus_key.Text = "-";
            this.minus_key.UseVisualStyleBackColor = true;
            this.minus_key.Click += new System.EventHandler(this.minus_key_Click);
            // 
            // multiply_key
            // 
            this.multiply_key.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.multiply_key.Location = new System.Drawing.Point(168, 129);
            this.multiply_key.Name = "multiply_key";
            this.multiply_key.Size = new System.Drawing.Size(40, 40);
            this.multiply_key.TabIndex = 14;
            this.multiply_key.Text = "*";
            this.multiply_key.UseVisualStyleBackColor = true;
            this.multiply_key.Click += new System.EventHandler(this.multiply_key_Click);
            // 
            // equal_key
            // 
            this.equal_key.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.equal_key.Location = new System.Drawing.Point(214, 175);
            this.equal_key.Name = "equal_key";
            this.equal_key.Size = new System.Drawing.Size(40, 86);
            this.equal_key.TabIndex = 16;
            this.equal_key.Text = "=";
            this.equal_key.UseVisualStyleBackColor = true;
            this.equal_key.Click += new System.EventHandler(this.equal_key_Click);
            // 
            // backspace_key
            // 
            this.backspace_key.Font = new System.Drawing.Font("Arial Black", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.backspace_key.ImageAlign = System.Drawing.ContentAlignment.TopRight;
            this.backspace_key.Location = new System.Drawing.Point(214, 129);
            this.backspace_key.Name = "backspace_key";
            this.backspace_key.Size = new System.Drawing.Size(40, 40);
            this.backspace_key.TabIndex = 17;
            this.backspace_key.Text = "BKSP";
            this.backspace_key.UseVisualStyleBackColor = true;
            this.backspace_key.Click += new System.EventHandler(this.backspace_key_Click);
            // 
            // clear_key
            // 
            this.clear_key.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.clear_key.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clear_key.ForeColor = System.Drawing.Color.Black;
            this.clear_key.Location = new System.Drawing.Point(214, 83);
            this.clear_key.Name = "clear_key";
            this.clear_key.Size = new System.Drawing.Size(40, 40);
            this.clear_key.TabIndex = 18;
            this.clear_key.Text = "C";
            this.clear_key.UseVisualStyleBackColor = false;
            this.clear_key.Click += new System.EventHandler(this.clear_key_Click);
            // 
            // CalculatorUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(271, 273);
            this.Controls.Add(this.clear_key);
            this.Controls.Add(this.backspace_key);
            this.Controls.Add(this.equal_key);
            this.Controls.Add(this.multiply_key);
            this.Controls.Add(this.minus_key);
            this.Controls.Add(this.plus_key);
            this.Controls.Add(this.point_key);
            this.Controls.Add(this.key_0);
            this.Controls.Add(this.key_6);
            this.Controls.Add(this.key_5);
            this.Controls.Add(this.key_4);
            this.Controls.Add(this.key_3);
            this.Controls.Add(this.key_2);
            this.Controls.Add(this.key_1);
            this.Controls.Add(this.display);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "CalculatorUI";
            this.Text = "Simple Calculator";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox display;
        private System.Windows.Forms.Button key_1;
        private System.Windows.Forms.Button key_2;
        private System.Windows.Forms.Button key_3;
        private System.Windows.Forms.Button key_4;
        private System.Windows.Forms.Button key_5;
        private System.Windows.Forms.Button key_6;
        private System.Windows.Forms.Button key_0;
        private System.Windows.Forms.Button point_key;
        private System.Windows.Forms.Button plus_key;
        private System.Windows.Forms.Button minus_key;
        private System.Windows.Forms.Button multiply_key;
        private System.Windows.Forms.Button equal_key;
        private System.Windows.Forms.Button backspace_key;
        private System.Windows.Forms.Button clear_key;

    }
}

