﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lutron.Swds.IntroToCSharp.Calculator {
    public class DivisionByZeroException : Exception {
        
        public override string Message {
            get {
                return "Division by zero";
            }
        }
    }
}
