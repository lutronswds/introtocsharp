﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lutron.Swds.IntroToCSharp.Calculator {
    public static class CharExtensions {
        /// <summary>
        /// Extension method for char that returns whether the argument is an operator char
        /// </summary>
        /// <param name="c"></param>
        /// <returns>True if character is a valid operator; False otherwise</returns>
        public static bool IsOperator(this char c) {
            return ((c == '+') || (c == '-') || (c == '*') || (c == '/'));
        }

        /// <summary>
        /// Extension method for char that returns whether the argument is an operand char
        /// </summary>
        /// <param name="c"></param>
        /// <returns>True if the character is a valid operand char; False otherwise</returns>
        public static bool IsOperand(this char c) {
            return ((char.IsDigit(c)) || (c == '.'));
        }
    }
}
