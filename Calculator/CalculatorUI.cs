﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lutron.Swds.IntroToCSharp.Calculator {
    public partial class CalculatorUI : Form {
        private bool blockInput = false;
        public CalculatorUI() {
            InitializeComponent();
            Calculator.Instance.expressionUpdated += UpdateDisplay;
        }

        private void ClearDisplay() {
            this.display.Text = "";
        }

        private void UpdateDisplay(String text) {
            this.ClearDisplay();
            this.display.Text = text;
        }
        
        private void Input(string text) {
            if (blockInput) {
                return;
            }
            Calculator.Instance.Input(text);
        }

        private void key_1_Click(object sender, EventArgs e) {
            Input("1");
        }     

        private void key_2_Click(object sender, EventArgs e) {
            Input("2");
        }

        private void key_3_Click(object sender, EventArgs e) {
            Input("3");
        }

        private void key_4_Click(object sender, EventArgs e) {
            Input("4");
        }

        private void key_5_Click(object sender, EventArgs e) {
            Input("5");
        }

        private void key_6_Click(object sender, EventArgs e) {
            Input("6");
        }        

        private void key_0_Click(object sender, EventArgs e) {
            Input("0");
        }

        private void point_key_Click(object sender, EventArgs e) {
            Input(".");
        }

        private void plus_key_Click(object sender, EventArgs e) {
            Input("+");
        }

        private void minus_key_Click(object sender, EventArgs e) {
            Input("-");
        }

        private void multiply_key_Click(object sender, EventArgs e) {
            Input("*");
        }
        
        private void backspace_key_Click(object sender, EventArgs e) {
            if (blockInput) { return; }
            Calculator.Instance.Backspace();
        }

        private void clear_key_Click(object sender, EventArgs e) {
            Calculator.Instance.Clear();
            blockInput = false;
        }

        private void equal_key_Click(object sender, EventArgs e) {
            if (blockInput) { return; }
            Calculator.Instance.Evaluate();
            blockInput = true;
        }
    }
}
