﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lutron.Swds.IntroToCSharp.Calculator {

    public delegate void ExpressionUpdateHandler(string expression);
    
    public class Calculator {

        public event ExpressionUpdateHandler expressionUpdated;
     
        private StringBuilder expression = new StringBuilder();
        private static Calculator calculator = null;
        private static object locker = new object();

        private void OnExpressionUpdated() {
            if (expressionUpdated != null) {
                expressionUpdated(expression.ToString());
            }
        }
        
        public static Calculator Instance{
            get {
                lock (locker) {
                    if (calculator == null) {
                        calculator = new Calculator();
                    }
                    return calculator;
                }
            }         
        }

        public void Input(string text) {
            expression.Append(text);
            OnExpressionUpdated();
        }

        internal void Backspace() {
            BackspaceExpression();
            OnExpressionUpdated();
        }

        private void BackspaceExpression() {
            if (expression.Length > 0) {
                expression.Remove(expression.Length - 1, 1);
            }
        }

        public void Clear() {
            expression.Clear();
            OnExpressionUpdated();
        }

        public float Evaluate() {            
            string operand1 = "";
            string operand2 = "";
            StringBuilder operandBuffer = new StringBuilder();
            char operatorChar = '\0';
            foreach (char c in expression.ToString()) {
                // what do you notice in below statement? recall presentation?
                if (c.IsOperand()) {
                    operandBuffer.Append(c);
                } else if (c.IsOperator()) { // similarly here, what do you notice?
                    operand1 = GetOperand(operandBuffer);
                    operatorChar = c;
                }
            }
            operand2 = GetOperand(operandBuffer);
            float result = EvaluateExpression(operatorChar, operand1, operand2);
            expression.Clear();
            expression.Append(result.ToString());
            OnExpressionUpdated();
            return result;
        }

        private float EvaluateExpression(char operatorChar, string operand1, string operand2) {
            float result = 0;
            float op1, op2;
            float.TryParse(operand1, out op1);
            float.TryParse(operand2, out op2);
            switch (operatorChar) {
                case '+':
                    result = op1 + op2;
                    break;
                case '-':
                    result = op1 - op2;
                    break;
                case '*':
                    result = op1 * op2;
                    break;
                default:
                    throw new InvalidOperationException("Invalid operation");      // throwing exception               
            }
            return result;
        }

        private static string GetOperand(StringBuilder operandBuffer) {
            string operand;
            operand = operandBuffer.ToString();
            operandBuffer.Clear();
            return operand;
        }
    }
}
