﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Lutron.Swds.IntroToCSharp.Calculator.Tests{
    [TestClass]
    public class CalculatorTest {
       
        [TestMethod]
        public void Test1() {
            //1+2 = 3
            Calculator.Instance.Clear();
            Calculator.Instance.Input("1");
            Calculator.Instance.Input("+");
            Calculator.Instance.Input("2");
            Assert.AreEqual(3, Calculator.Instance.Evaluate());
        }

        [TestMethod]
        public void Test2() {
            //1-2 = -1
            Calculator.Instance.Clear();
            Calculator.Instance.Input("1");
            Calculator.Instance.Input("-");
            Calculator.Instance.Input("2");
            Assert.AreEqual(-1, Calculator.Instance.Evaluate());
        }

        [TestMethod]
        public void Test3() {
            //2*4 = 8
            Calculator.Instance.Clear();
            Calculator.Instance.Input("2");
            Calculator.Instance.Input("*");
            Calculator.Instance.Input("4");
            Assert.AreEqual(8, Calculator.Instance.Evaluate());
        }       
    }
}
